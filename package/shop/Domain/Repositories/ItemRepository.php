<?php
namespace package\shop\Domain\Repositories;

use package\shop\Domain\Models\Item\Item;
use package\shop\Domain\Models\Item\ItemId;

interface ItemRepository
{
	public function findById(ItemId $id): Item;
	
	/**
	 * テスト用メソッド
	 * @param int $id
	 */
	public function find(int $id);
	
	/**
	 * 一覧を取得
	 */
	public function getList();
}
