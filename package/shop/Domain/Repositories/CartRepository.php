<?php
namespace package\shop\Domain\Repositories;
use package\shop\Domain\Models\Item\Item;
use package\shop\Domain\Models\Cart\CartElement;

interface CartRepository
{
	/**
	 * カートセッションの有無
	 */
	public function find();
	
	/**
	 * カート情報を取得する
	 */
	public function getCart();
	
	
	/**
	 * カートに追加する
	 */
	public function store(CartElement $cartElements);
	
	/**
	 * カートから削除する
	 */
	public function deleteCart(Item $item);
}

