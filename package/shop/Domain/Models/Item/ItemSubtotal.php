<?php
namespace package\shop\Domain\Models\Item;

use Prophecy\Exception\Prediction\PredictionException;

class ItemSubtotal
{
	private $value;

	public function __construct(int $value = 0)
	{
		if ($value < 0)
		{
			throw new PredictionException("value must be positive number". $value);
		}
		
		$this->value = $value;
	}
	
	public function value()
	{
		return $this->value;
	}

}