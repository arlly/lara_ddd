<?php
namespace package\shop\Domain\Models\Item;

use package\shop\Domain\Exception\PredictionException;

class ItemCount
{
	private $value;
	
	public function __construct(int $value)
	{
		if ($value <= 0)
		{
			throw new PredictionException("value must be positive number". $value);
		}
	
		$this->value = $value;
	}
	
	public function value()
	{
		return $this->value;
	}
}