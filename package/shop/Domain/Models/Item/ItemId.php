<?php
namespace package\shop\Domain\Models\Item;

class ItemId
{
	private $value;
	
	public function __construct(int $value)
	{
		$this->value = $value;
	}
	
	public function value()
	{
		return $this->value;
	}
	
}