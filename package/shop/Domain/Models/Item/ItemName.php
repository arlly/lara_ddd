<?php
namespace package\shop\Domain\Models\Item;

class ItemName
{
	private $value;
	
	public function __construct($value)
	{
		$this->value = $value;
	}
	
	public function value()
	{
		return $this->value;
	}
	
}