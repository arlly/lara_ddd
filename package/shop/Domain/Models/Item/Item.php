<?php

namespace package\shop\Domain\Models\Item;

class Item {
	private $id;
	private $name;
	private $price;
	private $stock;
	
	public function __construct(ItemId $id, $name, ItemPrice $price, ItemStock $stock) {
		$this->id = $id->value();
		$this->name = $name;
		$this->price = $price->value();
		$this->stock = $stock;
	}
	
	public function id() {
		return $this->id;
	}
	
	public function name() {
		return $this->name;
	}
	
	public function price() {
		return $this->price;
	}
	
	public function stock() {
		return $this->stock;
	}
}