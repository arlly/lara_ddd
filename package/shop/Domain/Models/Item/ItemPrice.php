<?php
namespace package\shop\Domain\Models\Item;

use Prophecy\Exception\Prediction\PredictionException;

//use package\shop\Domain\Models\Item\ItemSubtotal;
//use package\shop\Domain\Models\Item\ItemCount;

class ItemPrice
{
	private $value;

	public function __construct(int $value = 0)
	{
		if ($value < 0)
		{
			throw new PredictionException("value must be positive number". $value);
		}
		
		$this->value = $value;
	}
	
	public function multi(ItemCount $count): ItemSubtotal
	{
		return new ItemSubtotal($this->value * $count->value());
	}

	public function value()
	{
		return $this->value;
	}

}