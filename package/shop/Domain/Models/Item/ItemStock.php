<?php

namespace package\shop\Domain\Models\Item;

class ItemStock {
	private $value;
	public function __construct(int $value) {
		if ($value < 0) {
			throw new PredictionException ( "value must be positive number" . $value );
		}
		
		$this->value = $value;
	}
	
	public function value() {
		return $this->value;
	}
	
	/**
	 * 在庫のチェックをする
	 */
	public function isInsufficient(ItemCount $count)
	{
		
		if ($this->value < $count->value())
		{
			return true;
		}
	}
}