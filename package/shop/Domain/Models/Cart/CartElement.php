<?php

namespace package\shop\Domain\Models\Cart;

use package\shop\Domain\Repositories\CartRepository;
use package\shop\Domain\Models\Item\Item;
use package\shop\Domain\Models\Item\ItemCount;
use package\shop\DOmain\Models\Item\ItemPrice;
use package\shop\DOmain\Models\Item\ItemSubtotal;
use phpDocumentor\Reflection\Types\This;

class CartElement
{
  private $elements;
  
  private $item;
  private $count;
 
  public function __construct(Item $item, ItemCount $count)
  {
  	$this->item = $item;
  	$this->count = $count;
  	
  	
  	$elements =  [
  			"id" => $item->id(),
  			"name" => $item->name(),
  			"price" => $item->price(),
  			"count" => $count->value()
  	];
 
  	/*
  	$this->elements = [
  			"item" => $item,
  			"count" => $count
  	];
  	*/
  	
  	$this->elements = collect($elements);
  	
  	/**
  	 * 小計
  	 */
  	/*
  	$subtotal = $this->getSubTotal($count);
  	$this->elements["subtotal"] = $subtotal->value(); 
  	*/
  	
  }
  
  public function getElements()
  {
  	return $this->elements;
  }
  
  public function getSubTotal($count) : ItemSubtotal
  {
	$price = new ItemPrice ( $this->item->price () );
	$subtotal = $price->multi ( $count );
	
	return $subtotal;
  }

}