<?php

namespace package\shop\Domain\Models\Cart;

use package\shop\Domain\Models\Item\Item;
use package\shop\Domain\Models\Item\ItemCount;
use package\shop\Domain\Models\Item\ItemPrice;
use League\Flysystem\Exception;
use package\shop\Domain\Repositories\CartRepository;

class Cart {
	public function __construct($array = null) {
		$this->cart = $array;
		$this->setTotalPrice ();
	}
	
	public function addItem(Item $item, ItemCount $count) {
		
		if ($item->stock()->isInsufficient($count)) {
			throw new Exception ( "在庫が足りません！！" );
		}
		
		$this->elements = new CartElement ( $item, $count );
		return $this->elements;
	}
	
	public function getCart() {
		// print_r($this->cart);
		return $this->cart;
	}
	
	private function setTotalPrice() {
		$totalPrice = new CartTotalPrice ( $this->cart );
		$this->cart ["total_price"] = $totalPrice->value();
		//$this->cart ["total_price"] = $totalPrice;
	}
}