<?php
namespace package\shop\Domain\Models\Cart;

class CartTotalPrice
{
	private $value;
	
	public function __construct($array)
	{	
		$this->array = $array;
		$this->setValue();
	}
	
	private function setValue()
	{
		$price = 0;
		if(count($this->array))
		{
			foreach ($this->array as $array)
			{
				$price += $array["subtotal"];
			}
		}
		
		
		if ($price < 0)
		{
			throw new PredictionException("価格が不正です". $value);
		}
	
		$this->value = $price;
	}
	
	public function value()
	{
		return $this->value;
	}
	
}