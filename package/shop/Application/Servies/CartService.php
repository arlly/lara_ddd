<?php
namespace package\shop\Application\Servies;

use package\shop\Domain\Repositories\ItemRepository;
use package\shop\Domain\Repositories\CartRepository;

use package\shop\Domain\Models\Item\ItemId;
use package\shop\Domain\Models\Cart\Cart;
use package\shop\Domain\Models\Item\ItemCount;
use Intervention\Image\Exception\NotFoundException;

class CartService
{
	private $cartRepo;
	
	public function __construct(CartRepository $cartRepo)
	{
		$this->cartRepo = $cartRepo;
	}
	
	public function getCart():Cart
	{
		try{
			$cart = $this->cartRepo->find();
		}
		catch (NotFoundException $e)
		{
			$cart = new Cart();
		}
		
		return $cart;
	}
}