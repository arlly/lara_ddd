<?php
namespace package\shop\Application\Servies;

use package\shop\Domain\Repositories\ItemRepository;
class ItemService
{
	private $itemRepo;
	
	public function __construct(ItemRepository $itemRepo)
	{
		$this->itemRepo = $itemRepo;
	}
	
	public function getList()
	{
		return $this->itemRepo->getList();
	}
}