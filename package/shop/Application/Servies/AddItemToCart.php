<?php
namespace package\shop\Application\Servies;

use package\shop\Domain\Repositories\ItemRepository;
use package\shop\Domain\Models\Item\ItemId;
use package\shop\Domain\Models\Cart\Cart;
use package\shop\Domain\Models\Item\ItemCount;
use package\shop\Domain\Repositories\CartRepository;

use Intervention\Image\Exception\NotFoundException;
use package\shop\Domain\Exception\PredictionException;

use League\Flysystem\Exception;

class AddItemToCart
{
	private $itemRepo;
	
	public function __construct(ItemRepository $itemRepo, 
			                    CartRepository $cartRepo)
	{
		$this->itemRepo = $itemRepo;
		$this->cartRepo = $cartRepo;
	}
	
	public function add(int $itemId, int $count)
	{
		$item = $this->itemRepo->findById(new ItemId($itemId));
		
		
		try{
			$cart = $this->cartRepo->find();
		}
		catch (NotFoundException $e)
		{
			$cart = new Cart();
		}
		
		
		try {
			$elements = $cart->addItem($item, new ItemCount($count));
		}
		catch (PredictionException $e)
		{
			echo "不正な値です。";
			exit();
		}
		catch(Exception $e)
		{
			echo "在庫が不足しています";
			exit();
		}
		
		$this->cartRepo->store($elements);
	}
}