<?php
namespace package\shop\Infrastructure\Repositories\Cart;

use package\shop\Domain\Repositories\CartRepository;
use package\shop\Domain\Models\Item\Item;
use package\shop\Domain\Models\Cart\Cart;
use Illuminate\Http\Request;
use Intervention\Image\Exception\NotFoundException;
use package\shop\Domain\Models\Cart\CartElement;

class SessionCartRepository implements CartRepository
{
	public function __construct(Request $request)
	{
		$this->request = $request;
	}
	
	public function find()
	{
		
		
		if ($this->request->session()->has('cart')) {
			$this->cart = $this->request->session()->get('cart');
		}else{
			throw new NotFoundException("カートセッションが空です！");
			//$this->cart = array();
		}
		return $this->toDomain();
		
	}
	
	public function getCart()
	{
		return $this->request->session()->get('cart');
	}
	
	public function store(CartElement $cartElements)
	{
		
		$this->request->session()->push('cart', $cartElements->getElements());
	}
	
	public function deleteCart(Item $item)
	{
		
	}
	
	public function toDomain(): Cart
	{
		return new Cart($this->cart);
	}
}