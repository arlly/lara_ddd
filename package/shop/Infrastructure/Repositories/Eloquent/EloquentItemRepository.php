<?php
namespace package\shop\Infrastructure\Repositories\Eloquent;

use package\shop\Domain\Repositories\ItemRepository;
use package\shop\Infrastructure\Eloquents\EloquentItem;
use Intervention\Image\Exception\NotFoundException;

use package\shop\Domain\Models\Item\Item;
use package\shop\Domain\Models\Item\ItemId;

class EloquentItemRepository implements ItemRepository
{
	private $eloquent;
	
	public function __construct(EloquentItem $eloquent)
	{
		$this->eloquent = $eloquent;
	}
	
	
	public function findById(ItemId $id): Item
	{
		$item = $this->eloquent->find($id->value());
		if (empty($item))
		{
			throw new NotFoundException("item not Found");
		}
		
		return $item->toDomain();
	}
	
	
	/**
	 * テスト用のコードです
	 * {@inheritDoc}
	 * @see \package\shop\Domain\Repositories\ItemRepository::find()
	 */
	public function find(int $id)
	{
		$item = $this->eloquent->find($id);
		return $item;
	}
	
	public function getList()
	{
		return $this->eloquent->orderBy('id', 'desc')->paginate(5);
	}
}