<?php
namespace package\shop\Infrastructure\Eloquents;

use Illuminate\Database\Eloquent\Model;

use package\shop\Domain\Models\Item\Item;
use package\shop\Domain\Models\Item\ItemId;
use package\shop\Domain\Models\Item\ItemPrice;
use package\shop\Domain\Models\Item\ItemStock;


class EloquentItem extends Model
{
	protected $table = "items";
	
	public function __construct()
	{
		
	}
	
	public function setID()
	{
		
	}
	
	public function toDomain(): Item
	{
		return new Item(
				new ItemId($this->id),
				$this->name,
				new ItemPrice($this->price),
				new ItemStock($this->stock)
		);
	}
	
}