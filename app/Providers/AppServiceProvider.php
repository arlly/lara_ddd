<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\KeihiServiceInterface;
use App\Services\KeihiService;

//use App\package\shop\Domain\Repositories\ItemRepository;
//use App\package\shop\Infrastructure\Repositories\Eloquent\EloquentItemRepository;

use package\shop\Domain\Repositories\ItemRepository;
use package\shop\Infrastructure\Repositories\Eloquent\EloquentItemRepository;
use package\shop\Domain\Repositories\CartRepository;
use package\shop\Infrastructure\Repositories\Cart\SessionCartRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    	$this->app->singleton(KeihiServiceInterface::class, KeihiService::class);
    	$this->app->bind(ItemRepository::class, EloquentItemRepository::class);
    	$this->app->bind(CartRepository::class, SessionCartRepository::class);
    }
}
