<?php
namespace App\Services;

class CustomValidator extends \Illuminate\Validation\Validator
{
	public function validateFoo($attribute,$value,$parameters)
	{
		return false;
	}
	
	public function validateBoo($attribute,$value,$parameters)
	{
		return false;
	}
	
	public function validateCulcTotal($attribute,$value,$parameters)
	{
		$total = $this->getValue('bill_100') * 100
		          + $this->getValue('bill_50') * 50
		          + $this->getValue('bill_20') * 20
		          + $this->getValue('bill_10') * 10
		          + $this->getValue('bill_5') * 5
		          + $this->getValue('bill_1') * 1;
		
		if ($total != intval($value))
		{
			return false;
		}else 
		{
		    return true;	
		}
	}
}
