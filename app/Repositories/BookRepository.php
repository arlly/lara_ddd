<?php
namespace App\Repositories;

class BookRepository 
{
	protected $books = [
			[	
			  "id" => 1,
			  "title" => "Laravel リファレンス"
			]
	];
	
	public function getReferenceBooks()
	{
		return $this->books;
	}
	
	public function getBookIdTitle($id = null)
	{
		if (is_null($id))
		{
		  throw new \Exception();	
		}
		
		foreach ($this->books as $book)
		{
			if ($book["id"] == 1)
			{
				return $book;
			}
		}
		
		return null;
	}
	
	
}