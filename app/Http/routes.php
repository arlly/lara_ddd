<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
	$array = array(1 => "arimoto", 2 => "Fumimaro");
	//var_dump($array);
	
    return view('welcome');
});


/**
 * DDDの勉強ここから
 */
Route::get('cart',  'CartController@index');
Route::post('cart', 'CartController@addCart');
Route::get('cart/cart', 'CartController@cart');



Route::get('/vue', function () {
	return view('vue');
});

Route::get('/spa', function () {
	return view('spa');
});

Route::auth();

Route::get('/home', 'HomeController@index');
Route::resource('keihi', 'KeihiController');



Route::get('user', 'UserController@index');
Route::get('user/index', 'UserController@index');
Route::get('user/login', 'UserController@login');
Route::post('user/login', 'UserController@auth');
Route::get('user/logout', 'UserController@logout');

/**
 * Admin関連
 */
Route::get('admin',        'AdminController@index');
Route::get('admin/index',  'AdminController@index');
Route::post('admin/login', 'AdminController@auth');
Route::get('admin/login',  'AdminController@login');
Route::get('admin/logout', 'AdminController@logout');

//Route::group(['middleware' => ['guest:admin'], "prefix" => 'admin'], function () {

	
	
	/**
	 * テスト登録
	 */
	Route::post('/balance/add', 'AdminBalanceController@store');
	Route::get('/balance', 'AdminBalanceController@index');
	Route::get('/balance/add', 'AdminBalanceController@add');

//});