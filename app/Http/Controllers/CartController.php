<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use package\shop\Application\Servies\ItemService;
use package\shop\Application\Servies\AddItemToCart;
use package\shop\Application\Servies\CartService;

class CartController extends Controller
{
    //
    private $item_service;
    
    public function __construct(ItemService $item_service, 
    		                    CartService $cart_service)
    {
    	$this->item_service = $item_service;
    	$this->cart_service = $cart_service;
    }
    
    public function index()
    {
    	$results = $this->item_service->getList();
    	
    	return view('cart.index',  
    			    ['results'  => $results]);
    }
    
    public function cart()
    {
    	$cart = $this->cart_service->getCart();
    	
    	$results = $cart->getCart();
    	
    	return view('cart.cart',
    	            ['results'  => $results]);
    }
    
    public function addCart(Request $request, AddItemToCart $service)
    {
    	$this->validate($request, [
    			'item_id' => 'required|integer',
    			'count'   => 'required|integer'
    	]);
    	
    	$service->add($request->get('item_id'),
    			      $request->get('count'));
    	
    	return redirect("/cart/cart");
    }
}
