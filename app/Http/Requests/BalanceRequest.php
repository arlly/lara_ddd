<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BalanceRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
   public function rules()
    {
        return [
            //
        	'title'    => 'required|max:255',
        	'price'    => 'required|numeric|max:1000|culcTotal',
        	'bill_100' => 'numeric|max:1000|foo',
        	'bill_50'  => 'numeric|max:1000|boo',
       		'bill_20'  => 'numeric|max:1000',
       		'bill_10'  => 'numeric|max:1000',
       		'bill_5'   => 'numeric|max:1000',
       		'bill_1'   => 'numeric|max:1000',
        ];
    }
    
    public function attributes() {
    	return [
    			'title'    => 'タイトル',
        	    'price'    => '合計金額',
        	    'bill_100' => '$100紙幣',
        	    'bill_50'  => '$50紙幣',
       		    'bill_20'  => '$20紙幣',
       		    'bill_10'  => '$10紙幣',
       		    'bill_5'   => '$5紙幣',
       		    'bill_1'   => '$1紙幣',
    	];
    }
}
