<?php
namespace App;

use Illuminate\Filesystem\Filesystem;

class Report
{
	
	protected $file;
	
	public function __construct(Filesystem $file)
	{
		$this->file = $file;
	}
	
	public function output()
	{
		return $this->file->put('report.txt', 'report');
	}
}