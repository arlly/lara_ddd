<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keihi extends Model
{
    //
	protected $table = 'keihi';
	
	protected $fillable = [
			'title', 'price', 'url'
	];
}
