<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
    	Schema::create('items', function (Blueprint $table) {
    		$table->increments('id');       // ID
    		$table->char('name', 100);     // タイトル
    		$table->integer('price');       // 価格
    		$table->integer('stock');            // URL
    		$table->timestamps();           // 作成時刻
    	});
    	
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
