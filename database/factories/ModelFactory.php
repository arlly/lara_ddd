<?php

//use Faker;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/*
$factory->define(\App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});
*/


$factory->define(\App\Column::class, function (Faker\Generator $faker) {
	$faker = \Faker\Factory::create('ja_JP');
	
	return [
		'title' => $faker->firstNameFemale." ". $faker->lastName,
		'article' => $faker->streetName,
		'image' => $faker->address,
		'status' => 1,
		'contribute_date' => $faker->dateTime,
	];
})
;
