<?php

use Illuminate\Database\Seeder;
use App\Admin;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
    	for($i = 1; $i <= 50; $i ++) {
    		$admin = new Admin;
    		 
    		$admin->name1 = "テスト";
    		$admin->name2 = "太郎";
    		$admin->email = "admin{$i}@admin.com";
    		$admin->password = Hash::make("admin{$i}");
    		
    		 
    		$admin->save();
    	}
    }
}
