<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
    	//
    	//削除
    	User::truncate();
    	 
    	//User生成
    	for($i = 1; $i <= 50; $i ++) {
    		$user = new User;
    		 
    		$user->name1 = "テスト";
    		$user->name2 = "太郎";
    		$user->email = "user{$i}@user.com";
    		$user->password = Hash::make("user{$i}");
    		$user->company = "テスト株式会社";
    		$user->position = "テスト部署";
    		 
    		$user->save();
    	}
    	 
    	//保存
    	$user->save();
    }
}
