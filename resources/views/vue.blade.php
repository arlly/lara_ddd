@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                    <div class="panel-body">
                    <div id="app-3">
                      <p v-if="seen">Now you see me</p>
                    </div>
                    
                    <script>
                    var app3 = new Vue({
                    	  el: '#app-3',
                    	  data: {
                    	    seen: true
                    	  }
                    	})

                    </script>
                    
                    <div id="app-4">
                      <ol>
                        <li v-for="todo in todos">
                          @{{ todo.text }}
                        </li>
                      </ol>
                    </div>
                    <script>
                    var app4 = new Vue({
                    	  el: '#app-4',
                    	  data: {
                    	    todos: [
                    	      { text: 'Learn JavaScript' },
                    	      { text: 'Learn Vue' },
                    	      { text: 'in Laravel' },
                    	      { text: 'Build something awesome' }
                    	    ]
                    	  }
                    	})

                    app4.todos.push({ text: 'New item　後から追加' })
                    </script>
                </div>
                
                <div id="app-5">
                    <p>@{{ message }}</p>
                    <p>@{{ add_message }}</p>
                    <button v-on:click="reverseMessage">Reverse Message</button>
                    <button v-on:click="addMessage">Add Message</button>
                </div>
                
                <script>
                var app5 = new Vue({
                	  el: '#app-5',
                	  data: {
                	    message: 'Hello Vue.js!',
                	    add_message: ''
                	  },
                	  methods: {
                	    reverseMessage: function () {
                	      this.message = this.message.split('').reverse().join('')
                	    }
                	  },
                	  methods: {
                  	    addMessage: function () {
                  	      this.add_message = "これは後から追加しました！！"
                  	    }
                  	  }
                	})

                </script>
                
                <div id="app-6">
                  <p>@{{ message }}</p>
                  <input v-model="message">
                </div>
                
                <script type="text/javascript">
                var app6 = new Vue({
                	  el: '#app-6',
                	  data: {
                	    message: 'Hello Vue!'
                	  }
                	})

                </script>
                
                <div id="app-7">           
                  <ol>
                    <!-- todo オブジェクトによって各 todo-item を提供します。それは、内容を動的にできるように表します。-->
                    <todo-item v-for="item in groceryList" v-bind:todo="item"></todo-item>
                  </ol>
                </div>
                
                <script>
                Vue.component('todo-item', {
                	  // todo-item コンポーネントはカスタム属性のような "プロパティ" で受け取ります。
                	  // このプロパティは todo と呼ばれます。
                	  props: ['todo'],
                	  template: '<li>@{{ todo.text }}</li>'
                	})
               
                var app7 = new Vue({
                  el: '#app-7',
                  data: {
                    groceryList: [
                      { text: 'Vegetables' },
                      { text: 'Cheese' },
                      { text: 'Whatever else humans are supposed to eat' }
                    ]
                  }
                })

                </script>
                
                <div  id="app-8" v-bind:class="classObject">test</div>
                
                <script>
                var app8 = new Vue({
                	el: '#app-8',
                	data: {
                		  isActive: true,
                		  error: null
                		},
                		computed: {
                		  classObject: function () {
                		    return {
                		      active: this.isActive && !this.error,
                		      'text-danger': this.error && this.error.type === 'fatal',
                		    }
                		  }
                		}

                });
                

                </script>
                
                <div id="example-1" class="demo">
                  <input v-model="message" type="number" placeholder="edit me">
                    <p>Message is: @{{ message }}</p>
                </div>
                
                <script>
                  new Vue({
                    el: '#example-1',
                    data: {
                      message: ''
                    }
                  })
                </script>
                
                <div id="example-2">
                  <button v-on:click="counter += 1">Add 1</button>
                  <p>The button above has been clicked @{{ counter }} times.</p>
                </div>
                
                <script>
                var example2 = new Vue({
                	  el: '#example-2',
                	  data: {
                	    counter: 0
                	  }
                	})
                </script>
                
                <div id="example-3">
                  <button v-on:click="say('hi')">Say hi</button>
                  <button v-on:click="say('what')">Say what</button>
                </div>
                
                <script>
                new Vue({
                	  el: '#example-3',
                	  methods: {
                	    say: function (message) {
                	      alert(message)
                	    }
                	  }
                	})

                </script>
                
                

                
                
            </div>
        </div>
    </div>
</div>
@endsection
