@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        {!! $results->render() !!}
            
            <h2>コラム一覧</h2>
            <h3>{{ $results->count() }}件のコラムがあります！</h3>
            @foreach($results as $result)
            <div class="well">
              <p><h4>{{$result->title}}</h4></p>
              <p>
              <a href="{{ url('/column/') }}/{{ $result->id }}/detail">
              {{ Common::cutString(strip_tags($result->article), 100) }}
              </a>
              </p>
            </div>
            
        
            @endforeach
            
            
        </div>
    </div>
</div>
@endsection
