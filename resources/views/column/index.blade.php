@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row"> 
          
        {!! $results->render() !!}
        
        @foreach($results as $result)
        <h3>{{ $result->title }}</h3>
        
        
        
        
        <div class="well">
        
        
        @if ($result->image)
          @if ($result->width)
            <p><img src="/images/column/{{$result->image}}" style="width:{{$result->width}}px; height:{{$result->height}}px"></p>
          @else
            <p><img src="/images/column/{{$result->image}}" style="width:100%;"></p>
          @endif
            
        @endif
        <p>{!! nl2br($result->article) !!}</p>
        </div>
        
        <div class="well">
        <p>{{ $result->contribute_date }}</p>
        </div>
        
        @endforeach
 
            
            
        </div>
    </div>
</div>
@endsection
