@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">       
        
        
        
        <h3>{{ $row->title }}</h3>
        
        
        
        
        <div class="well">
        @if ($row->image)
            <p><img src="/images/column/{{$row->image}}" style="width:100%;"></p>
        @endif
        <p>{!! nl2br($row->article) !!}</p>
        </div>
        
        <div class="well">
        <p>{{ $row->contribute_date }}</p>
        </div>
        
        
            
            
        </div>
    </div>
</div>
@endsection
