@extends('layouts.admin')

@section('content')
<div class="container">
  <h2>バランスセット追加</h2>
  
  
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    
    
    
  {!! Form::open(array('files' => true)) !!}
  
    <div class="form-group">
      {!! Form::label('title', 'タイトル:') !!}
      {!! Form::text('title', null, ['class' => 'form-control ckeditor']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('price', '金額:') !!}
      {!! Form::text('price', null, ['class' => 'form-control ckeditor']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('bill_100', '$100:') !!}
      {!! Form::text('bill_100', null, ['class' => 'form-control ckeditor']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('bill_50', '$50:') !!}
      {!! Form::text('bill_50', null, ['class' => 'form-control ckeditor']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('bill_20', '$20:') !!}
      {!! Form::text('bill_20', null, ['class' => 'form-control ckeditor']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('bill_10', '$10:') !!}
      {!! Form::text('bill_10', null, ['class' => 'form-control ckeditor']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('bill_5', '$5:') !!}
      {!! Form::text('bill_5', null, ['class' => 'form-control ckeditor']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('bill_1', '$1:') !!}
      {!! Form::text('bill_1', null, ['class' => 'form-control ckeditor']) !!}
    </div>
    

    
    
    <!--  
    <div class="form-group">
      {!! Form::label('ステータス', 'ステータス:') !!}
    
      {!! Form::radio('status', '1', true) !!}有効　
      {!! Form::radio('status', '0') !!}　無効
    </div>
    -->
    
    
    <div class="form-group">
      {!! Form::submit('送信', ['class' => 'btn btn-primary form-control']) !!}
    </div>
  
  {!! Form::close() !!}
</div>
@endsection
