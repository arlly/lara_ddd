@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
            	Welcome!! {{ Auth::guard('admin')->user()->name1 }} {{ Auth::guard('admin')->user()->name2 }} 
            </div>
        </div>
    </div>
</div>
@endsection
