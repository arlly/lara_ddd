@extends('layouts.admin')

@section('content')
<div class="container">
    
    <h2>商品情報一覧</h2>
    
    
    
    @if (Session::has('flash_message'))
      <div class="alert alert-success">{{ Session::get('flash_message') }}</div>
    @endif
    
    {{-- エラーの表示を追加 --}}
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    
    {!! Form::open(array('files' => true)) !!}
    <div class="panel panel-primary">
      <div class="panel-heading">検索する</div>
      <div class="panel-body">
        <div class="form-group email required user_basic_email">
          <label class="email required control-label" for="user_basic_email">
               　　　　　　　　
          </label>
          <input class="form-control" id="search" name="search" placeholder="メーカー、製品名から検索">
        </div>
      <input class="btn btn-default" name="commit" type="submit" value="検索">
      </div>
    </div>
    {!! Form::close() !!}
    
    {!! $results->render() !!}
    　　　　<div class="pagination"><a href="{{ url('/admin/product/add') }}" class="btn btn-primary">商品情報を追加する</a></div>
      
      <table  class="table">
		<tr>
			<td>画像</td>
        	<td>メーカー名</td>
        	<td>製品名</td>
           	<td>編集</td>
        	<td>削除</td>

        </tr>
    @foreach($results as $result)
        <tr>
            <td><img src="/images/product/200-{{ $result->image }}"></td>
        	<td>{{ $result->maker }}</td>
        	<td>{{ $result->productName }}</a></td>
        	
        	<!--<td>{!! link_to(action('AdminNewsController@edit', [$result->id]), '編集', ['class' => 'btn btn-primary']) !!}</td>-->
        	<td><a href="{{ url('/admin/product/') }}/{{ $result->id }}/edit" class="btn btn-primary">編集</a></td>
			<td><a href="{{ url('/admin/product/') }}/{{ $result->id }}/delete" class="btn btn-primary" data-toggle="confirmation" onclick="window.confirm('本当に削除しますか？')">削除</a></td>
        </tr>

    @endforeach
    </table>
    
</div>
@endsection
