@extends('layouts.admin')

@section('content')
<div class="container">
  <h2>商品情報追加</h2>
  
  {{-- エラーの表示を追加 --}}
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    
    
    
  {!! Form::open(array('files' => true)) !!}
  
    <div class="form-group">
      {!! Form::label('maker', 'メーカー名:') !!}
      {!! Form::text('maker', $row->maker, ['class' => 'form-control']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('productName', '製品名:') !!}
      {!! Form::text('productName', $row->productName, ['class' => 'form-control']) !!}
    </div>
    
   <div class="form-group">
      {!! Form::label('image', '外観画像:') !!}
      {!! Form::file('image', null) !!}
@if ($row->image)
      <img src="/images/product/200-{{$row->image}}">
@endif
   </div>
   
   <div class="form-group">
      {!! Form::label('sellSituation', '発売状況:') !!}
      {!! Form::select('sellSituation', $arrParam['sellSituation'], $row->sellSituation, ['class' => 'form-control']) !!}
   </div>
   
   <div class="form-group">
      {!! Form::label('price', '価格:') !!}
      {!! Form::text('price', $row->price, ['class' => 'form-control']) !!}
   </div>
        
    <div class="form-group">
      {!! Form::label('feature', '製品の特長:') !!}
      {!! Form::textarea('feature', $row->feature, ['class' => 'form-control ckeditor']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('element', '想定要素:') !!}
      {!! Form::select('element', $arrParam['element'], $row->element, ['class' => 'form-control']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('methodPrinting', '印刷方式:') !!}
      {!! Form::select('methodPrinting', $arrParam['methodPrinting'], $row->methodPrinting, ['class' => 'form-control']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('colorMaterial', '利用色材:') !!}
      {!! Form::select('colorMaterial', $arrParam['colorMaterial'], $row->colorMaterial, ['class' => 'form-control']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('methodTransport', '用紙搬送方式:') !!}
      {!! Form::select('methodTransport', $arrParam['methodTransport'], $row->methodTransport, ['class' => 'form-control']) !!}
    </div>
   
   <div class="form-group">
      {!! Form::label('printSpeed', '印刷速度/搬送速度:') !!}
      {!! Form::text('printSpeed', $row->printSpeed, ['class' => 'form-control']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('resolution', '出力解像度:') !!}
      {!! Form::text('resolution', $row->resolution, ['class' => 'form-control']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('colors', '印刷色数:') !!}
      {!! Form::text('colors', $row->colors, ['class' => 'form-control']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('media', '対応媒体種類:') !!}
      {!! Form::text('media', $row->media, ['class' => 'form-control']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('paperPressure', '対応用紙圧/用紙坪量/用紙斤量:') !!}
      {!! Form::text('paperPressure', $row->paperPressure, ['class' => 'form-control']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('paperSize', '対応用紙サイズ:') !!}
      {!! Form::text('paperSize', $row->paperSize, ['class' => 'form-control']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('printSize', '印刷サイズ:') !!}
      {!! Form::text('printSize', $row->printSize, ['class' => 'form-control']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('outputNumber', '月間標準出力枚数:') !!}
      {!! Form::text('outputNumber', $row->outputNumber, ['class' => 'form-control']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('controller', 'コントローラ種類:') !!}
      {!! Form::text('controller', $row->controller, ['class' => 'form-control']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('changeColor', '色数変更:') !!}
      {!! Form::text('changeColor', $row->changeColor, ['class' => 'form-control']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('optionPaper', '給紙オプション:') !!}
      {!! Form::text('optionPaper', $row->optionPaper, ['class' => 'form-control']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('optionProcessing', '加工オプション:') !!}
      {!! Form::text('optionProcessing', $row->optionProcessing, ['class' => 'form-control']) !!}
    </div>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    <!--  
    <div class="form-group">
      {!! Form::label('Image1') !!}
      {!! Form::file('image1', null) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('Image2') !!}
      {!! Form::file('image2', null) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('Image3') !!}
      {!! Form::file('image3', null) !!}
    </div>
    
    -->
    
    <div class="form-group">
      {!! Form::submit('送信', ['class' => 'btn btn-primary form-control']) !!}
    </div>
  
  {!! Form::close() !!}
</div>
@endsection
