@extends('layouts.admin')

@section('content')
<div class="container">
    
    <h2>コラム一覧</h2>
    
    @if (Session::has('flash_message'))
      <div class="alert alert-success">{{ Session::get('flash_message') }}</div>
    @endif
    

    
    
    {!! $results->render() !!}
    　　　　<div class="pagination"><a href="{{ url('/admin/column/add') }}" class="btn btn-primary">コラムを追加する</a></div>
      
      <table  class="table">
		<tr>
			<td>ID</td>
        	<td>日付</td>
        	<td>タイトル</td>
           	<td>編集</td>
        	<td>削除</td>

        </tr>
    @foreach($results as $result)
        <tr>
        	<td>{{ $result->id }}</td>
        	<td>{{ $result->contribute_date }}</td>
        	<td>{{ $result->title }}</a></td>
        	<td><a href="{{ url('/admin/column/') }}/{{ $result->id }}/edit" class="btn btn-primary">編集</a></td>
        	<td><a href="{{ url('/admin/column/') }}/{{ $result->id }}/delete" class="btn btn-primary" data-toggle="confirmation" onclick="window.confirm('本当に削除しますか？')">削除</a></td>
        </tr>

    @endforeach
    </table>
    
</div>
@endsection
