@extends('layouts.admin')

@section('content')
<div class="container">
  <h2>コラム追加</h2>
  
  
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    
    
    
  {!! Form::open(array('files' => true)) !!}
  
    <div class="form-group">
      {!! Form::label('title', 'タイトル:') !!}
      {!! Form::text('title', null, ['class' => 'form-control ckeditor']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('image', '画像:') !!}
      {!! Form::file('image', null) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('article', '本文:') !!}
      {!! Form::textarea('article', null, ['class' => 'form-control ckeditor']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('width', '横幅:') !!}
      {!! Form::text('width', null, ['class' => 'form-control ckeditor']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('height', '高さ:') !!}
      {!! Form::text('height', null, ['class' => 'form-control ckeditor']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('contribute_date', '投稿日時:') !!}
      {!! Form::text('contribute_date', date("Y-m-d H:i:s"), ['class' => 'form-control ckeditor']) !!}
    </div>
    
    
    <!--  
    <div class="form-group">
      {!! Form::label('ステータス', 'ステータス:') !!}
    
      {!! Form::radio('status', '1', true) !!}有効　
      {!! Form::radio('status', '0') !!}　無効
    </div>
    -->
    
    
    <div class="form-group">
      {!! Form::submit('送信', ['class' => 'btn btn-primary form-control']) !!}
    </div>
  
  {!! Form::close() !!}
</div>
@endsection
