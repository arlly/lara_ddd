@extends('layouts.admin')

@section('content')
<div class="container">
  <h2>パスワード変更</h2>
  
  {{-- エラーの表示を追加 --}}
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    
    
    
  {!! Form::open() !!}
  
      
    <div class="form-group">
      {!! Form::label('password', '新しいパスワード:') !!}
      {!! Form::text('password', null, ['class' => 'form-control ckeditor']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('password_confirmation', '新しいパスワード(確認用):') !!}
      {!! Form::text('password_confirmation', null, ['class' => 'form-control ckeditor']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::submit('変更', ['class' => 'btn btn-primary form-control']) !!}
    </div>
  
  {!! Form::close() !!}
</div>
@endsection
