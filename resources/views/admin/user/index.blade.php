@extends('layouts.admin')

@section('content')
<div class="container">
    
    <h2>ユーザー情報一覧</h2>
    
    @if (Session::has('flash_message'))
      <div class="alert alert-success">{{ Session::get('flash_message') }}</div>
    @endif
    
    {!! Form::open(array('files' => true)) !!}
    <div class="panel panel-primary">
      <div class="panel-heading">検索する</div>
      <div class="panel-body">
        <div class="form-group email required user_basic_email">
          <label class="email required control-label" for="user_basic_email">
               　　　　　　　　
          </label>
          <input class="form-control" id="search" name="search" placeholder="ユーザー、メールアドレスから検索">
        </div>
      <input class="btn btn-default" name="commit" type="submit" value="検索">
      </div>
    </div>
    {!! Form::close() !!}
    
    
    {!! $results->render() !!}
    　　　　<div class="pagination"><a href="{{ url('/admin/user/add') }}" class="btn btn-primary">ユーザーを追加する</a></div>
      
      <table  class="table">
		<tr>
			<td>ID</td>
        	<td>会社名</td>
        	<td>お名前</td>
        	<td>メールアドレス</td>
           	<td>編集</td>
           	<td>パスワード変更</td>
        	<td>削除</td>

        </tr>
    @foreach($results as $result)
        <tr>
        	<td>{{ $result->id }}</td>
        	<td>{{ $result->company }}</a></td>
        	<td>{{ $result->name1 }} {{ $result->name2 }}</a></td>
        	<td>{{ $result->email }}</a></td>
        	
        	
        	<td><a href="{{ url('/admin/user/') }}/{{ $result->id }}/edit" class="btn btn-primary">編集</a></td>
        	<td><a href="{{ url('/admin/user/') }}/{{ $result->id }}/changePassword" class="btn btn-primary">変更</a></td>
			<td><a href="{{ url('/admin/user/') }}/{{ $result->id }}/delete" class="btn btn-primary" data-toggle="confirmation" onclick="window.confirm('本当に削除しますか？')">削除</a></td>
        </tr>

    @endforeach
    </table>
    
</div>
@endsection
