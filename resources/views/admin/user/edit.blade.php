@extends('layouts.admin')

@section('content')
<div class="container">
  <h2>ユーザー追加</h2>
  
  {{-- エラーの表示を追加 --}}
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    
    
    
  {!! Form::open() !!}
  
    <div class="form-group">
      {!! Form::label('name', 'お名前:') !!}<br />
               姓:{!! Form::text('name1', $row->name1, ['class' => 'form-control']) !!}
               名:{!! Form::text('name2', $row->name2, ['class' => 'form-control']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('company', '会社名:') !!}
      {!! Form::text('company', $row->company, ['class' => 'form-control ckeditor']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('position', '役職:') !!}
      {!! Form::text('position', $row->position, ['class' => 'form-control ckeditor']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('email', 'メールアドレス:') !!}
      {!! Form::text('email', $row->email, ['class' => 'form-control ckeditor']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('ステータス', 'ログインステータス:') !!}
      {!! Form::radio('status', '1', ($row->status == 1) ? true : false ) !!}有効　
      {!! Form::radio('status', '0', ($row->status == 0) ? true : false ) !!}　無効
    </div>
    
    <div class="form-group">
      {!! Form::submit('送信', ['class' => 'btn btn-primary form-control']) !!}
    </div>
  
  {!! Form::close() !!}
</div>
@endsection
