<html>
<head>
    <title>一覧</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.11.8/semantic.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.11.8/semantic.min.js"></script>
</head>
<body>
<div class="ui main container">
    <div class="ui segment">
    <h1>商品一覧</h1>
    {{link_to('/cart/cart', 'カートへ')}}
    
    
    
    @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
           </div>
    @endif
           
    {!! $results->render() !!}
    <table class="ui celled table">
        <thead>
        <tr>
            <th>商品ID</th>
            <th>商品名</th>
            <th>価格</th>
            <th>在庫数</th>
            <th>注文数</th>
            <th>カートへ入れる</th>
        </tr>
        </thead>
        <tbody>
@foreach($results as $item)
{!! Form::open(['class' => 'form-horizontal']) !!}
  {{ Form::hidden("item_id", $item->id) }}
            <tr>
                <td>{{{ $item->id }}}</td>
                <td>{{{ $item->name }}}</td>
                <td>{{{ $item->price }}}</td>
                <td>{{{ $item->stock }}}</td>
                <td>{!! Form::text('count', null, ['class' => 'form-control', 'maxlength' => '2']) !!}</td>
                <td>
                    <button type="submit" class="btn btn-primary">
						<span class="glyphicon glyphicon-ok"></span>&nbsp;カートへいれる
					</button>
                </td>
            </tr>
{!! Form::close() !!}
@endforeach
        </tbody>
    </table>
    </div>
</div>
</body>
</html>