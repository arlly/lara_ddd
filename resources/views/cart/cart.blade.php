<html>
<head>
    <title>一覧</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.11.8/semantic.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.11.8/semantic.min.js"></script>
</head>
<body>
<div class="ui main container">
    <div class="ui segment">
    <h1>カート</h1>
    @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
           </div>
    @endif
           
    {{link_to('/cart', '商品一覧へ')}}
    <table class="ui celled table">
        <thead>
        <tr>
            <th>商品ID</th>
            <th>商品名</th>
            <th>価格</th>
            <th>注文数</th>
            <th>小計</th>
        </tr>
        </thead>
        <tbody>
@foreach($results as $item)
            <tr>
                <td>{{{ $item["id"] }}}</td>
                <td>{{{ $item["name"] }}}</td>
                <td>{{{ $item["price"] }}}</td>
                <td>{{{ $item["count"] }}}</td>
                <td>{{{ $item["subtotal"] }}}</td>
            </tr>
@endforeach
            <tr>
                <td colspan="4">合計</td>
                <td>{{{ $results["total_price"] }}}</td>
            </tr>
        </tbody>
    </table>
    </div>
</div>
</body>
</html>