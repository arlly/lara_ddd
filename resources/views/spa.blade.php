@extends('layouts.app')

@section('content')

<script>
$(function(){
	  var hoge = Vue.extend({template: '#hoge'});
	  var fuga = Vue.extend({template: '#fuga'});
	  var App = Vue.extend({});
	  var router = new VueRouter();
	  router.map({
	    '/hoge': { component: hoge},
	    '/fuga': { component: fuga}
	  });
	  router.start(App, '#app');
	});
</script>
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                    <div class="panel-body">
                      <div id="app">
                      <!-- リンク先を `to` プロパティに指定します -->
                      <!-- デフォルトで <router-link> は `<a>` タグとしてレンダリングされます -->
                      <div>
  <a v-link="/hoge">hogeへ</a>
  <a v-link="/fuga">fugaへ</a>
  <router-view></router-view>
</div>
<div id="hoge" style="display:none;">
  hoge page
</div>
<div id="fuga" style="display:none;">
  fuga page
</div>
                      </div>
                    </div>
                    
                
                

                
                
            </div>
        </div>
    </div>
</div>
@endsection
