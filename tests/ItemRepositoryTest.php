<?php
//use app\package\shop\Domain\Repositories\ItemRepository;

use package\shop\Domain\Repositories\ItemRepository;

use package\shop\Domain\Models\Item\ItemId;
use package\shop\Domain\Models\Cart\Cart;
use package\shop\Domain\Models\Item\ItemCount;
use package\shop\Domain\Models\Item\Item;


use package\shop\Domain\Repositories\CartRepository;

class ItemRepositoryTest extends TestCase
{

	protected $repo;

	protected function setUp()
	{
		parent::setUp();
		
		//$this->repo = $this->app->make(\App\package\shop\Domain\Repositories\ItemRepository::class);
		$this->repo = $this->app->make(ItemRepository::class);
		
	}

	
	
	/**
	 * @test
	 */
	public function 商品のタイトルの返却をテスト() {
		$result = $this->repo->find(1);
		$this->assertSame('FXC-70H MGS', $result->name);
		
	}
	
	/**
	 * @test
	 */
	public function 商品エンティティを返却するテスト()
	{
		$ItemId = new ItemId(1);
		$result = $this->repo->findByID($ItemId);
		$this->assertSame('FXC-70H MGS', $result->name());
	}
	
	
	/**
	 * @test
	 */
	public function 小計を求めるテスト()
	{
		$item = $this->repo->findByID(new ItemId(1));
		
		$cart = new Cart();
		$subtotal = $cart->addItem($item, new ItemCount(5));
		$this->assertEquals(220000, $subtotal->value());
	}
	
	
	/**
	 * @test
	 */
	public function 在庫を超える発注で例外を出すテスト()
	{
		$item = $this->repo->findByID(new ItemId(1));
		
		$cart = new Cart();
		$item = $this->repo->findByID(new ItemId(1));
		try {
			$subtotal = $cart->addItem($item, new ItemCount(100));
		}catch (Exception $e)
		{
			$this->assertEquals(0, $e->getCode());
		}
	}
	
	
	
}