<?php
class ColumnDummyTest extends TestCase
{
	use \Illuminate\Foundation\Testing\DatabaseMigrations;
	
	public function testDummy()
	{
		factory(\App\Column::class, 500)->create();
		$this->assertEquals(45, \App\Column::all()->count());
	}
}