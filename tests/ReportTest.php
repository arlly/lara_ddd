<?php
use Mockery;
class ReportTest extends \PHPUnit_Framework_TestCase 
{
	protected $report;
	
	/*
	protected function setUp()
	{
		$this->report = new \App\Report(new \Illuminate\Filesystem\Filesystem());
	}
	*/
	
	public function tearDown()
	{
		Mockery::close();
	}
	
	public function testOutput() { 
		//$this->assertSame(6, $this->report->output());
		
		$filesystemMock = Mockery::mock('Illuminate\Filesystem\Filesystem');
		$content = 'report';
		$filesystemMock->shouldReceive('put')->with('report.txt', $content)->once()->andReturn(strlen($content));
		$report = new \App\Report($filesystemMock);
		$this->assertSame(6, $report->output());
	}
}

